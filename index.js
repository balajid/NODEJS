var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var neo4j = require('neo4j-driver').v1;


var app=express();

app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname,'public')));

const driver = neo4j.driver('bolt://localhost',neo4j.auth.basic('neo4j','graph'));

const session= driver.session();

app.get('/home',function(req,res){
  var d=new Date(1526052537000);
  console.log("hiii"+d.getDate()+"  "+(d.getMonth()+1)+"  "+d.getFullYear());
    res.redirect('/');
});

app.get('/entry',function(req,res){

      res.render('entry');

});
app.get('/status',function(req,res){

    session.run('MATCH(n:flights) return n')
           .then(function(result){
                flightsarr=[];
                var matcherarr=[];
                result.records.forEach(function(record){
                      console.log(record._fields[0].properties);
                      flightsarr.push({
                          id: record._fields[0].identity.low,
                          flightname: record._fields[0].properties.Flightname,
                          company: record._fields[0].properties.company
                      })

                });

                session.run('MATCH(n:airports) return n')
                       .then(function(result){
                         aiportarr=[];
                         result.records.forEach(function(record){
                              aiportarr.push({
                                id: record._fields[0].identity.low,
                                airportname: record._fields[0].properties.Airportname,
                                city: record._fields[0].properties.city,
                                country: record._fields[0].properties.country
                              })
                         });
                         res.render('status',{
                              flight:flightsarr,
                              airport:aiportarr,
                         });

                       }).catch(function(err){
                            console.log(err);
                       });


           })
           .catch(function(err){
                console.log(err);
           });

});
app.get('/',function(req,res){
      res.render('index');

});
app.post("/node/add",function(req,res){
    const name=req.body.flightname;
    const company=req.body.company;
    //console.log(req.body.psw);
    session
          .run('create(n:flights {Flightname:{nameParam},company:{comParam}}) return n',{nameParam:name,comParam:company})
          .then(function(result){
            console.log('message added');
            res.redirect('/entry');
            session.close();
          })
          .catch(function(err){
              console.log(err);
          });
});

app.post("/node/fadd",function(req,res){
    const name=req.body.airportname;
    const city=req.body.city;
    const country=req.body.country;
    session
          .run('create(n:airports {Airportname:{nameParam},city:{ciParam},country:{coParam}}) return n',{nameParam:name,ciParam:city,coParam:country})
          .then(function(result){
            console.log('airport added');
            res.redirect('/entry');
            session.close();
          })
          .catch(function(err){
              console.log(err);
          });
});

app.post("/node/set",function(req,res){
    const pname=req.body.flightname;
    const fname=req.body.depname;
    session
          .run('match(a:flights {Flightname:{pnameParam}}),(b:airports{Airportname:{fnameParam}}) MERGE(a)-[r:Departs]->(b) return a,b',{pnameParam:pname,fnameParam:fname})
          .then(function(result){
            console.log('relation added');
            res.redirect('/entry');
            session.close();
          })
          .catch(function(err){
              console.log(err);
          });
});
app.listen(3000);
console.log('server started');
